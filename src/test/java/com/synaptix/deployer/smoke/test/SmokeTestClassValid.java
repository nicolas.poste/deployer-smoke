package com.synaptix.deployer.smoke.test;

import com.synaptix.deployer.smoke.SmokeTestCase;

/**
 * Created by NicolasP on 09/06/2016.
 */
public class SmokeTestClassValid extends SmokeTestCase {

	public SmokeTestClassValid() {
		super("SmokeTestSuccess");
	}

	@SmokeTest(description = "test")
	public void test1() {

	}

	@SmokeTest(description = "test2")
	public void test2() {

	}
}
