package com.synaptix.deployer.smoke.test;

import org.junit.Assert;
import org.junit.Test;

import com.synaptix.deployer.smoke.exception.SmokeDeserializationException;
import com.synaptix.deployer.smoke.model.SmokeTestReport;

/**
 * Created by NicolasP on 14/06/2016.
 */
public class SmokeReportDeserializerTest {

	@Test
	public void testDeserializer() {
		String s = "{\"remainingTests\":[],\"testsDone\":[{\"name\":\"Smoke test for integrator\",\"smokeTests\":[{\"name\":\"isStarted\",\"description\":\"Test if the integrator is started\",\"state\":\"VALID\"},{\"name\":\"areAgentsInitialized\",\"description\":\"Check if the agents are initialized\",\"state\":\"VALID\"}],\"message\":null,\"state\":\"VALID\"}],\"testsValid\":true,\"reportDate\":\"14/06/2016 06:52:17\"}";

		SmokeReportDeserializer smokeReportDeserializer = new SmokeReportDeserializer();
		SmokeTestReport smokeTestReport = null;
		try {
			smokeTestReport = smokeReportDeserializer.deserialize(s);
		} catch (SmokeDeserializationException e) {
			Assert.fail(e.getMessage());
		}

		System.out.println(smokeTestReport);

		Assert.assertNotNull(smokeTestReport.getTestsDone());
		Assert.assertFalse(smokeTestReport.getTestsDone().isEmpty());
	}
}
