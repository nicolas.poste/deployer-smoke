package com.synaptix.deployer.smoke.test;

import com.synaptix.deployer.smoke.SmokeTestCase;

/**
 * Created by NicolasP on 09/06/2016.
 */
public class SmokeTestClassFail extends SmokeTestCase {

	public SmokeTestClassFail() {
		super("SmokeTestFail");
	}

	@SmokeTest(description = "test")
	public void test1() {

	}

	@SmokeTest(description = "test2")
	public void test2() {
		fail("test2 has successfully failed!");
	}
}
