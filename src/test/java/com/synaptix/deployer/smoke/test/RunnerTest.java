package com.synaptix.deployer.smoke.test;

import org.junit.Assert;
import org.junit.Test;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.synaptix.deployer.smoke.SmokeRunner;
import com.synaptix.deployer.smoke.model.SmokeTestReport;

/**
 * Created by NicolasP on 09/06/2016.
 */
public class RunnerTest {

	@Test
	public void testRunner() {
		SmokeRunner smokeRunner = new SmokeRunner();
		SmokeTestReport smokeTestReport = smokeRunner.testAll();

		Assert.assertFalse(smokeTestReport.isTestsValid());

		SmokeReportDeserializer deserializer = new SmokeReportDeserializer();
		ObjectMapper mapper = deserializer.getMapper();
		mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL); // for tests

		String s = null;
		try {
			s = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(smokeTestReport);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}

		System.out.println(s);

		SmokeTestReport smokeTestReport2 = null;
		try {
			smokeTestReport2 = deserializer.deserialize(s);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail();
		}
		Assert.assertNotNull(smokeTestReport2);
		Assert.assertEquals(smokeTestReport.isTestsValid(), smokeTestReport2.isTestsValid());
		Assert.assertEquals(smokeTestReport.getRemainingTests(), smokeTestReport2.getRemainingTests());
		Assert.assertEquals(smokeTestReport.getTestsDone(), smokeTestReport2.getTestsDone());
		Assert.assertEquals(smokeTestReport.getReportDate(), smokeTestReport2.getReportDate());

	}
}
