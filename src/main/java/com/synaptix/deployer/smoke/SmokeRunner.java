package com.synaptix.deployer.smoke;

import java.lang.reflect.Method;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.reflections.Reflections;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;

import com.synaptix.deployer.smoke.model.SmokeTest;
import com.synaptix.deployer.smoke.model.SmokeTestCaseResultBuilder;
import com.synaptix.deployer.smoke.model.SmokeTestReport;
import com.synaptix.deployer.smoke.model.SmokeTestReportBuilder;
import com.synaptix.deployer.smoke.model.SmokeTestState;

/**
 * Created by NicolasP on 09/06/2016.
 */
public class SmokeRunner {

	private static final Log LOG = LogFactory.getLog(SmokeRunner.class);

	private final Set<Class<? extends SmokeTestCase>> smokeTests;

	/**
	 * Create a smoke runner which will run smoke tests over your application<br>
	 * Each test class must extend {@link com.synaptix.deployer.smoke.SmokeTestCase}
	 */
	public SmokeRunner() {
		Reflections reflections = new Reflections(new ConfigurationBuilder().setUrls(ClasspathHelper.forPackage("")).setScanners(new SubTypesScanner()));

		smokeTests = reflections.getSubTypesOf(SmokeTestCase.class);
	}

	/**
	 * Test all test cases. Returns a report
	 */
	public SmokeTestReport testAll() {
		SmokeTestReportBuilder reportBuilder = new SmokeTestReportBuilder();
		for (Class<? extends SmokeTestCase> smokeTestCaseClass : smokeTests) {
			testCase(smokeTestCaseClass, reportBuilder);
		}
		return reportBuilder.build();
	}

	private void testCase(Class<? extends SmokeTestCase> smokeTestCaseClass, SmokeTestReportBuilder reportBuilder) {
		SmokeTestCaseResultBuilder smokeTestResultBuilder = null;
		String name = null;
		try {
			SmokeTestCase smokeTestCase = createTestCase(smokeTestCaseClass);
			name = smokeTestCase.getName();
			smokeTestResultBuilder = reportBuilder.addReport(name);

			boolean allValid = test(smokeTestCase, smokeTestCaseClass, smokeTestResultBuilder);
			if (allValid) {
				smokeTestResultBuilder.success();
			} else {
				smokeTestResultBuilder.fail();
			}
		} catch (Exception e) {
			if (smokeTestResultBuilder != null) {
				smokeTestResultBuilder.fail();
			}
			LOG.error(String.format("Error launching test %s", name != null ? name : smokeTestCaseClass.getSimpleName()), e);
		}
	}

	/**
	 * Create a test case. Default implementation uses {@link java.lang.Class#getDeclaredConstructor(Class[]).newInstance(Object[]))}
	 */
	protected SmokeTestCase createTestCase(Class<? extends SmokeTestCase> smokeTestCaseClass) throws Exception {
		return smokeTestCaseClass.getDeclaredConstructor().newInstance();
	}

	private boolean test(SmokeTestCase smokeTestCase, Class<? extends SmokeTestCase> clazz, SmokeTestCaseResultBuilder smokeTestResultBuilder) {
		boolean allValid = true;
		SmokeTest lastTest = null;
		try {
			smokeTestCase.setUp();

			Map<SmokeTest, Method> methodMap = new LinkedHashMap<>();
			for (Method method : clazz.getMethods()) {
				SmokeTestCase.SmokeTest annotation = method.getAnnotation(SmokeTestCase.SmokeTest.class);
				if (annotation != null) {
					methodMap.put(smokeTestResultBuilder.newTest(method.getName(), annotation.description()), method);
				}
			}
			for (Map.Entry<SmokeTest, Method> entry : methodMap.entrySet()) {
				lastTest = entry.getKey();
				smokeTestCase.beforeTest();
				entry.getValue().invoke(smokeTestCase);
				entry.getKey().setState(SmokeTestState.VALID);
				smokeTestCase.afterTest();
			}
		} catch (Error | Exception e) {
			allValid = false;
			if (lastTest != null) {
				lastTest.setState(SmokeTestState.INVALID);
			}
			if (e.getCause() instanceof AssertionError) {
				smokeTestResultBuilder.message(String.format("Assertion error while running test case %s (%s), test %s: %s", smokeTestCase.getName(), clazz.getSimpleName(), lastTest != null ? lastTest.getName() : "unknown", e.getCause().getMessage()));
			} else {
				smokeTestResultBuilder.message(String.format("Exception occurred while running test case %s (%s), test %s", smokeTestCase.getName(), clazz.getSimpleName(), lastTest != null ? lastTest.getName() : "unknown"));
			}
			if (LOG.isTraceEnabled()) {
				LOG.trace(String.format("Exception with %s (%s)", smokeTestCase.getName(), clazz.getSimpleName()), e);
			}
		} finally {
			smokeTestCase.tearDown();
		}
		return allValid;
	}
}
