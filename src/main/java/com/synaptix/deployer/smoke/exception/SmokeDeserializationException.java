package com.synaptix.deployer.smoke.exception;

/**
 * Created by NicolasP on 13/06/2016.
 */
public class SmokeDeserializationException extends Exception {

	public SmokeDeserializationException(String message, Throwable cause) {
		super(message, cause);
	}
}
