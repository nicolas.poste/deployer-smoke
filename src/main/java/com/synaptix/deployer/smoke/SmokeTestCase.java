package com.synaptix.deployer.smoke;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Any class extending this one will be part of a smoke test campaign.<br>
 * Methods annotated with {@link SmokeTest} will be called
 * Created by NicolasP on 09/06/2016.
 */
public abstract class SmokeTestCase {

	private final String name;

	/**
	 * Create a new smoke test case named as specified
	 */
	public SmokeTestCase(String name) {
		this.name = name;
	}

	final String getName() {
		return name;
	}

	/**
	 * setUp is called upon the creation of the test case
	 */
	protected void setUp() {
	}

	/**
	 * beforeTest is called before each test annotated with {@link SmokeTest}
	 */
	protected void beforeTest() {
	}

	/**
	 * afterTest is called after each test annotated with {@link SmokeTest}
	 */
	protected void afterTest() {

	}

	/**
	 * tearDown is called at the end of the test case, whether is has successfully finished or not
	 */
	protected void tearDown() {
	}

	/**
	 * Raises an AssertionError with given message
	 */
	protected final void fail(String message) {
		if (message == null) {
			throw new AssertionError();
		}
		throw new AssertionError(message);
	}

	/**
	 * Each test must be annotated with @SmokeTest<br>
	 * A description can be provided to explain the reader what the test does
	 */
	@Target(ElementType.METHOD)
	@Retention(RUNTIME)
	public @interface SmokeTest {

		/**
		 * The name of the smoke test
		 */
		String description() default "";

	}
}
