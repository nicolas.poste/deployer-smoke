package com.synaptix.deployer.smoke.model;

/**
 * Created by NicolasP on 09/06/2016.
 */
public enum SmokeTestState {

	VALID, INVALID, PENDING
}
