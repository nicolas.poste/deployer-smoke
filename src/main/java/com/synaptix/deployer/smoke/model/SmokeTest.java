package com.synaptix.deployer.smoke.model;

/**
 * This object contains the result of a unique test<br>
 * Created by NicolasP on 09/06/2016.
 */
public final class SmokeTest {

	private static final String LINE_SEPARATOR = System.lineSeparator();

	private String name;

	private String description;

	private SmokeTestState state;

	private SmokeTest() { // for deserialization only
		this(null, null);
	}

	SmokeTest(String name, String description) {
		this.name = name;
		this.description = description;
		this.state = SmokeTestState.PENDING;
	}

	public SmokeTestState getState() {
		return state;
	}

	public void setState(SmokeTestState state) {
		this.state = state;
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("name=").append(name).append(LINE_SEPARATOR);
		sb.append("description=").append(description).append(LINE_SEPARATOR);
		sb.append("state=").append(state);
		return sb.toString();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		SmokeTest smokeTest = (SmokeTest) o;

		if (!name.equals(smokeTest.name)) {
			return false;
		}
		if (!description.equals(smokeTest.description)) {
			return false;
		}
		return state == smokeTest.state;

	}

	@Override
	public int hashCode() {
		int result = name.hashCode();
		result = 31 * result + description.hashCode();
		result = 31 * result + state.hashCode();
		return result;
	}
}
