package com.synaptix.deployer.smoke.model;

import java.util.List;

/**
 * A full campaign report is stored in this object<br>
 * Created by NicolasP on 09/06/2016.
 */
public final class SmokeTestReport {

	private static final String LINE_SEPARATOR = System.lineSeparator();

	private SmokeTestReport data;

	private List<SmokeTestCaseResult> remainingTests;

	private List<SmokeTestCaseResult> testsDone;

	private boolean testsValid;

	private String reportDate;

	public SmokeTestReport getData() {
		return data;
	}

	public void setData(SmokeTestReport data) {
		this.data = data;
	}

	public List<SmokeTestCaseResult> getRemainingTests() {
		return remainingTests;
	}

	void setRemainingTests(List<SmokeTestCaseResult> remainingTests) {
		this.remainingTests = remainingTests;
	}

	public List<SmokeTestCaseResult> getTestsDone() {
		return testsDone;
	}

	public void setTestsDone(List<SmokeTestCaseResult> testsDone) {
		this.testsDone = testsDone;
	}

	public boolean isTestsValid() {
		return testsValid;
	}

	void setTestsValid(boolean testsValid) {
		this.testsValid = testsValid;
	}

	public String getReportDate() {
		return reportDate;
	}

	void setReportDate(String reportDate) {
		this.reportDate = reportDate;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("Report generated at ").append(getReportDate()).append(LINE_SEPARATOR);
		sb.append("valid=").append(isTestsValid()).append(LINE_SEPARATOR);
		if (testsDone != null) {
			int m = testsDone.size();
			for (int i = 0; i < m; i++) {
				sb.append(String.format("\t-- Test Case %d/%d --%n", i + 1, m));
				SmokeTestCaseResult smokeTestResult = testsDone.get(i);
				sb.append(smokeTestResult.toString().replaceAll("\n", "\n\t").replaceAll("^", "\t")).append(LINE_SEPARATOR);
			}
		}
		if (remainingTests != null) {
			int m = remainingTests.size();
			for (int i = 0; i < m; i++) {
				sb.append(String.format("\t-- Unfinished Test Case %d/%d --%n", i + 1, m));
				SmokeTestCaseResult remainingTest = remainingTests.get(i);
				sb.append(remainingTest.toString().replaceAll("\n", "\n\t").replaceAll("^", "\t")).append(LINE_SEPARATOR);
			}
		}
		sb.append("End of report");
		return sb.toString();
	}
}
