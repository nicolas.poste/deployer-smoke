package com.synaptix.deployer.smoke.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * This object contains the result of a test case<br>
 * Created by NicolasP on 09/06/2016.
 */
final class SmokeTestCaseResult {

	private static final String LINE_SEPARATOR = System.lineSeparator();
	private final List<SmokeTest> smokeTests;
	private String name;
	private String message;

	private SmokeTestState state;

	private SmokeTestCaseResult() { // for deserialization only
		this(null);
	}

	SmokeTestCaseResult(String name) {
		this.name = name;
		this.smokeTests = new ArrayList<>();
		this.state = SmokeTestState.PENDING;
	}

	public String getName() {
		return name;
	}

	public String getMessage() {
		return message;
	}

	void setMessage(String message) {
		this.message = message;
	}

	public SmokeTestState getState() {
		return state;
	}

	void setState(SmokeTestState state) {
		this.state = state;
	}

	void add(SmokeTest smokeTest) {
		smokeTests.add(smokeTest);
	}

	public List<SmokeTest> getSmokeTests() {
		return Collections.unmodifiableList(smokeTests);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("name=").append(name).append(LINE_SEPARATOR);
		sb.append("message=").append(message).append(LINE_SEPARATOR);
		sb.append("state=").append(state);
		int m = smokeTests.size();
		for (int i = 0; i < m; i++) {
			sb.append(String.format("%n\t- Test %d/%d -", i + 1, m));
			if (sb.length() > 0) {
				sb.append(LINE_SEPARATOR);
			}
			SmokeTest smokeTest = smokeTests.get(i);
			sb.append(smokeTest.toString().replaceAll("\n", "\n\t").replaceAll("^", "\t"));
		}
		return sb.toString();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		SmokeTestCaseResult that = (SmokeTestCaseResult) o;

		if (!name.equals(that.name)) {
			return false;
		}
		if (!smokeTests.equals(that.smokeTests)) {
			return false;
		}
		if (message != null ? !message.equals(that.message) : that.message != null) {
			return false;
		}
		return state == that.state;

	}

	@Override
	public int hashCode() {
		int result = name.hashCode();
		result = 31 * result + smokeTests.hashCode();
		result = 31 * result + (message != null ? message.hashCode() : 0);
		result = 31 * result + state.hashCode();
		return result;
	}
}
