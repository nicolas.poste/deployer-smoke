package com.synaptix.deployer.smoke.model;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * A full campaign report is build using this object<br>
 * Created by NicolasP on 09/06/2016.
 */
public final class SmokeTestReportBuilder {

    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss").withZone(ZoneId.systemDefault());

    private static final Log LOG = LogFactory.getLog(SmokeTestReportBuilder.class);

    private final List<SmokeTestCaseResultBuilder> smokeTestResultBuilders;

    private final List<SmokeTestCaseResult> smokeTestResults;

    public SmokeTestReportBuilder() {
        this.smokeTestResultBuilders = new ArrayList<>();
        this.smokeTestResults = new ArrayList<>();
    }

    /**
     * Add a new report corresponding to a whole new case
     */
    public SmokeTestCaseResultBuilder addReport(String name) {
        SmokeTestCaseResultBuilder smokeTestResultBuilder = new SmokeTestCaseResultBuilder(name, this);
        smokeTestResultBuilders.add(smokeTestResultBuilder);
        return smokeTestResultBuilder;
    }

    void complete(SmokeTestCaseResultBuilder smokeTestResultBuilder) {
        if (smokeTestResultBuilders.remove(smokeTestResultBuilder)) {
            smokeTestResults.add(smokeTestResultBuilder.getSmokeTestResult());
        } else {
            LOG.error(String.format("The report %s has already been completed", smokeTestResultBuilder.getName()));
        }
    }

    private String getReportDate() {
        return formatter.format(Instant.now());
    }

    public boolean isTestsValid() {
        if (!smokeTestResultBuilders.isEmpty()) {
            return false;
        }
        return smokeTestResults.stream()
                .noneMatch(smokeTestResult -> smokeTestResult.getState() != SmokeTestState.VALID);
    }

    public List<SmokeTestCaseResult> getRemainingTests() {
        return smokeTestResultBuilders.stream()
                .map(SmokeTestCaseResultBuilder::getSmokeTestResult)
                .collect(Collectors.toList());
    }

    public List<SmokeTestCaseResult> getSmokeTestResults() {
        return smokeTestResults;
    }

    public SmokeTestReport build() {
        SmokeTestReport smokeTestReport = new SmokeTestReport();
        smokeTestReport.setReportDate(getReportDate());
        smokeTestReport.setTestsValid(isTestsValid());
        smokeTestReport.setTestsDone(smokeTestResults);
        smokeTestReport.setRemainingTests(getRemainingTests());
        return smokeTestReport;
    }
}
