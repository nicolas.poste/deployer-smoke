package com.synaptix.deployer.smoke.model;

import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Created by NicolasP on 09/06/2016.
 */
public final class SmokeTestCaseResultBuilder {

	private static final Log LOG = LogFactory.getLog(SmokeTestCaseResultBuilder.class);

	final SmokeTestCaseResult smokeTestResult;

	final SmokeTestReportBuilder reportBuilder;

	final Map<String, SmokeTest> smokeTests;

	/**
	 * Created by {@link SmokeTestReportBuilder#addReport(String)}
	 */
	SmokeTestCaseResultBuilder(String name, SmokeTestReportBuilder reportBuilder) {
		this.smokeTestResult = new SmokeTestCaseResult(name);
		this.reportBuilder = reportBuilder;
		this.smokeTests = new LinkedHashMap<>();
	}

	public SmokeTest newTest(String name, String description) {
		SmokeTest smokeTest = new SmokeTest(name, description);
		SmokeTest previous = smokeTests.put(name, smokeTest);
		if (previous != null) {
			LOG.warn(String.format("There are two tests or more which are using the same name %s!", name));
		}
		smokeTestResult.add(smokeTest);
		return smokeTest;
	}

	public SmokeTestCaseResultBuilder message(String message) {
		this.smokeTestResult.setMessage(message);
		return this;
	}

	SmokeTestCaseResult getSmokeTestResult() {
		return smokeTestResult;
	}

	String getName() {
		return smokeTestResult.getName();
	}

	String getMessage() {
		return smokeTestResult.getMessage();
	}

	@Override
	public String toString() {
		return smokeTestResult.toString();
	}

	public void success() {
		smokeTestResult.setState(SmokeTestState.VALID);

		reportBuilder.complete(this);
	}

	public void fail() {
		smokeTestResult.setState(SmokeTestState.INVALID);

		reportBuilder.complete(this);
	}
}
