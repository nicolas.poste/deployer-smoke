# Introduction

Ce projet permet de lancer des tests ["Smoke"](https://en.wikipedia.org/wiki/Smoke_testing_(software) sur votre application.
Combinés avec le [déployeur](https://gitlab.talanlabs.com/nicolas-poste/deployer-cmder), ils seront joués à chaque déploiement. Voir la notice côté déployeur pour plus d'informations sur comment les combiner.

# Dépendance

```xml
		<dependency>
			<groupId>com.synaptix.deployer</groupId>
			<artifactId>deployer-smoke</artifactId>
			<version>1.0.1</version>
		</dependency>
```

# Création des classes de test

Afin de définir des cas de tests, il faut créer des classes qui étendent la classe abstraite `SmokeTestCase`. Ces classes sont construites par SmokeRunner, qu'il est possible d'étendre, notamment pour surcharger la création des classes.

Ces classes doivent être localisées dans les sources java, et non dans les tests (ils ne seraient pas déployés sur l'application sinon). C'est notamment pour cette raison que les tests "Smoke" ne doivent que consulter l'état de l'application, sans en changer l'état ou sa base de données.

Par défaut, les classes sont utilisées avec `Class.getDeclaredConstructor().newInstance()`, ce qui peut s'avérer insuffisant. Voir la partie *Utilisation avec Guice*

Dans cette classe de test, les méthodes annotées avec `@SmokeTest` seront alors appelées lors d'une campagne de test.

# Exposition dans une servlet

Afin de pouvoir jouer ces tests, il est intéressant de les exposer dans une servlet rest. Exemple :
SmokeTestReport 
```java
@Path("/smoke")
public class SmokeRest {

	@Inject
	private SmokeRunner smokeRunner; // on pourrait récupérer une instance de SmokeRunner via d'autres façons

	@GET
	@Path("/run")
	@Produces(MediaType.APPLICATION_JSON)
	public SmokeTestReport run() {
		return smokeRunner.testAll();
	}
}
```

La méthode `testAll()` retourne un rapport de tests.

Exemple :
```json
{
	"remainingTests":[

	],
	"testsDone":[
		{
			"name":"Smoke test for integrator",
			"smokeTests":[
				{
					"name":"isStarted",
					"description":"Test if the integrator is started",
					"state":"VALID"
				},
				{
					"name":"areAgentsInitialized",
					"description":"Check if the agents are initialized",
					"state":"VALID"
				}
			],
			"message":null,
			"state":"VALID"
		}
	],
	"testsValid":true,
	"reportDate":"14/06/2016 07:44:39"
}
```

# Utilisation avec Guice

```java
public class GuiceSmokeRunner extends SmokeRunner {

	@Inject
	private Injector injector;

	@Override
	protected SmokeTestCase createTestCase(Class<? extends SmokeTestCase> smokeTestCaseClass) throws Exception {
		return injector.getInstance(smokeTestCaseClass);
	}
}
```

Ne pas oublier de binder cette classe :
```java
		// Smoke
		bind(SmokeRunner.class).to(GuiceSmokeRunner.class).in(Singleton.class);
		bind(GuiceSmokeRunner.class).in(Singleton.class);
```

# Tests automatisés

Il est également possible de lancer un test automatisé qui lance les tests "Smoke" :
```java
public class ITSmoke {

	private static final Log LOG = LogFactory.getLog(ITSmoke.class);

	@Inject
	private SmokeRunner smokeRunner;

	@Test
	public void runSmokeTests() {
		
		try {
			// start server here
	
			SmokeTestReport smokeTestReport = smokeRunner.testAll();

			if (LOG.isDebugEnabled()) {
				LOG.debug(smokeTestReport);
			}

			Assert.assertFalse(smokeTestReport.getTestsDone().isEmpty());

			Assert.assertTrue(smokeTestReport.isTestsValid());
		} finally {
			// stop server here
		}
	}
}
```
